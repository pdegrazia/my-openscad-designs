$fn = 100;
lengthSpacebar = 15;
widthSpacebar = 118.10; 
scaleSpacebar = 0.98;

lengthKeycap = 15;
widthKeycap = 15; 
scaleKeycap = 0.8;


module keycap(length, width, scale, steps){
//stem, stays always the same
    for (i = steps){
        translate([0,i])
        difference() {
            cylinder(r=2.8, h=5, center=true);        
            cube([5.6,1.3,10], center=true);
            cube([1.3,5.6,10], center=true);
        }
    }
    //truncated pyramid
    translate([0,0,0.5])
    difference(){
        linear_extrude(height=2.5, scale=scale)
            square([length, width], center=true);

        color("red")
        linear_extrude(height=1.5, scale=scale)
            square([length - 1, width - 1], center=true);
    }

    //bottom part of the pyramid
    translate([0,0,-0.25])
    difference(){
        cube([length,width,1.5], center=true);
        cube([length -1, width - 1, 1.5], center=true);

    }
}

//keycap(lengthSpacebar, widthSpacebar, scaleSpacebar, [-50, 0, 50]);
keycap(lengthKeycap, widthKeycap, scaleKeycap, [0]);
