module heart(){
    scale([0.13,0.13])
    import("heart.svg", center=true);
}

module hollowHeart(){
    difference(){
        heart();

        color("red")
            offset(r=-1)
                heart();
    }
}

module mytext(){
    rotate([0,0,-5])
    translate([-5.5,-2.5])
            text("N", size=7, font="Waltograph UI");

    translate([-0.6,0])
            text("M", size=5.9, font="Waltograph UI");
}

//color("red")
scale([2,2,1])
linear_extrude(2){
    hollowHeart();
    mytext();
}

translate([-15,15])
difference(){
        cylinder(r=4, h=2, $fn=100);
        cylinder(r=2, h=2, $fn=100);    
    
}