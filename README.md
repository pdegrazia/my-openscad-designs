# My OpenScad designs

Simple test repo to host my openscad designs

## Flat keycaps for cherry mx switches, parametric

Print upside down (stem up, surface down)

usage

```
// spacebar
keycap(lengthSpacebar, widthSpacebar, scaleSpacebar, [-50, 0, 50]);

//plain keycap
keycap(lengthKeycap, widthKeycap, scaleKeycap, [0]);
```

## Nora and Mila Keyring
simple heart shaped keyring

## Logo
my silly logo